--
-- Created by IntelliJ IDEA.
-- User: insai
-- Date: 7/7/2016
-- Time: 9:55 PM
-- To change this template use File | Settings | File Templates.
--
local L = LibStub("AceLocale-3.0"):GetLocale("KindredArtifactTracking", true);
local LA = LibStub:GetLibrary("LibArtifactData-1.0")
local KL = LibStub:GetLibrary("KindredLibs")

KindredArtifactTracking = LibStub("AceAddon-3.0"):NewAddon("KindredArtifactTracking", "AceConsole-3.0", "AceEvent-3.0", "AceTimer-3.0", "AceComm-3.0", "AceSerializer-3.0")

KATRACK_Options = {
    name = "Kindred Artifact Tracking",
    handler = KindredArtifactTracking,
    type = "group",
    args = {
        showOnTooltip = {
            type = "toggle",
            name = "Show on Tooltip",
            desc = "Show artifact information on character tooltips.",
            set = "SetValue",
            get = "GetValue"
        },
        showOnPaperDoll = {
            type = "toggle",
            name = "Show on Character Sheet",
            desc = "Show your current artifact's information on your character sheet.'",
            set = "SetValue",
            get = "GetValue"
        },
        enableDebug = {
            type = "toggle",
            name = "Enable Debug Logging",
            desc = "Enable the logging of events for the purposes of gathering debug information.'",
            set = "SetValue",
            get = "GetValue"
        },
        addonMessageDelay = {
            type = "input",
            name = "Message Delay (Seconds)",
            desc = "The delay between sending messages to other players with the addon in seconds.",
            set = "SetValue",
            get = "GetValue",
            pattern = "%d",
            usage = "Must be an integer value"
        },
        clearCache = {
            type = "execute",
            name = "Clear Cache",
            desc = "Clear out your artifact cache.",
            func = "ClearCache",
            confirm = true,
            confirmText = "This will clear out all cached artifact information, are you sure?"
        },
        showCache = {
            type = "execute",
            name = "Show Cache",
            desc = "Show the artifact information you currently have cached.",
            func = "ShowCachedArtifacts"
        },
        showDebugInfo = {
            type = "execute",
            name = "Show Debug Panel",
            desc = "Show any logged debug information.",
            func = "ShowDebugPanel"
        },
        clearDebug = {
            type = "execute",
            name = "Clear Cached Debug Information",
            desc = "Clear out your debug cache.",
            func = "ClearDebug",
            confirm = true,
            confirmText = "This will clear out all cached debug information, are you sure?"
        }
    }
}

local sendTargets = {}
local requestTargets = {}
local playersWithAddon = {}
local messageQueue = {}
local receivedMessages = {}
local lastBroadcastHaveAddon = {
    RAID = 0,
    GUILD = 0
}

local cacheFrame = CreateFrame("Frame", nil, UIParent)
cacheFrame:Hide()

local debugFrame = CreateFrame("Frame", nil, UIParent)
debugFrame:Hide()

local messageDetailFrame = CreateFrame("Frame", nil, UIParent)
messageDetailFrame:Hide()

local function len(iterable)
    local count = 0
    for k,v in pairs(iterable) do
        count = count + 1
    end
    return count
end

-- Options Getters and Setters
function KindredArtifactTracking:GetValue(info)
	local db = self.db.profile
    return db[info[1]]
end

function KindredArtifactTracking:SetValue(info,v)
	local db = self.db.profile
    if info[1] == "showOnPaperDoll" then
        if v then
            self:RegisterPaperdoll()
        else
            self:UnregisterPaperdoll()
        end
    end
    db[info[1]] = v
end

KUTIL_prefix = "KUTIL_"
KUTIL_prefixArtifacts = "artifacts"
KUTIL_prefixRequestArtifacts = "rArtifacts"

function KindredArtifactTracking:UpdateArtifacts()
    if self.MyUnitGUID == nil then
        self.MyUnitGUID = self:NameToGUID(UnitName("player"))
    end
    if self.MyUnitGUID == nil or InCombatLockdown() then return false end
    local myCurrentArtifacts = self.db.global.artifactCache[self.MyUnitGUID]
    if myCurrentArtifacts == nil then
        myCurrentArtifacts = LA.GetAllArtifactsInfo()
    else
        local toBeCached = LA.GetAllArtifactsInfo()
        for k, v in pairs(toBeCached) do
            if k ~= 'knowledgeLevel' and k ~= 'knowledgeMultiplier' then
                myCurrentArtifacts[k] = v
            end
        end
    end
    local ak, akm = LA.GetArtifactKnowledge()
    myCurrentArtifacts['knowledgeLevel'] = ak
    myCurrentArtifacts['knowledgeMultiplier'] = akm
    self.db.global.artifactCache[self.MyUnitGUID] = myCurrentArtifacts
end

function KindredArtifactTracking:GetCurrentArtifactInfo()
    if self.MyUnitGUID == nil then
        self.MyUnitGUID = self:NameToGUID(UnitName("player"))
    end
    if self.MyUnitGUID == nil then return false end
    local myCacheEntry = self.db.global.artifactCache[self.MyUnitGUID]
    if myCacheEntry then
        local myArtifactInfo = self.db.global.artifactCache[self.MyUnitGUID][LA.GetActiveArtifactID()]
        if myArtifactInfo then
            local name = myArtifactInfo['name']
            local traits = myArtifactInfo['numRanksPurchased']
            local goldTraits = self:GetGoldTraitCount(myArtifactInfo['traits'])
            local knowledge = {}
            knowledge['level'] = myArtifactInfo['knowledgeLevel']
            knowledge['multiplier'] = myArtifactInfo['knowledgeMultiplier']
            return name, traits, knowledge, goldTraits
        else
            return false
        end
    end
end

function KindredArtifactTracking:GetArtifactInfo(id)
    if self.MyUnitGUID == nil then
        self.MyUnitGUID = self:NameToGUID(UnitName("player"))
    end
    if self.MyUnitGUID == nil then return false end
    local myArtifactInfo = self.db.global.artifactCache[self.MyUnitGUID][id]
    local name = myArtifactInfo['name']
    local traits = myArtifactInfo['numRanksPurchased']
    return name, traits
end

function KindredArtifactTracking:ClearCache()
    self.db.global.artifactCache = {}
end

function KindredArtifactTracking:ClearDebug()
    receivedMessages = {}
end

function KindredArtifactTracking:GetArtifactWeapons(guid, unit)
    if not self:IsGUID(guid, 'Player') then return false; end

    if self.db.global.artifactCache[guid] then
        weapons = {}
        knowledge = {}
        for k, v in pairs(self.db.global.artifactCache[guid]) do
            if k ~= 'knowledgeLevel' and k ~= 'knowledgeMultiplier' and k ~= 'lastUpdated' then
                table.insert(weapons, v)
            else
                knowledge[k] = v
            end
        end
        -- If our artifact information for this target (not ourself) is older than 5 minutes, request new information from the target.
        if unit ~= nil and UnitName(unit) ~= UnitName('player') and self.db.global.artifactCache[guid].lastUpdated ~= nil and time() - self.db.global.artifactCache[guid].lastUpdated > 300 then
            if self.db.profile.enableDebug then
                local debugInfo = {
                    from = 'Self - Diagnostic',
                    pfx = "N/A",
                    dist = "N/A",
                    msg = "Sending Request for updated artifact information.",
                    rcvd = time()
                }
                receivedMessages[len(receivedMessages) + 1] = debugInfo
            end
            self:SendArtifactRequestToTarget(unit)
        end
        return weapons, knowledge
    else
        return {}, {}
    end
end

function KindredArtifactTracking:GetGoldTraitCount(traits)
    local goldCount = 0
    if traits ~= nil then
        for k, v in pairs(traits) do
            if v.isGold then goldCount = goldCount + 1 end
        end
    end
    return goldCount
end

--
-- Credit where credit is due - The following functions were borrowed from SimpleILevel by Scotepi
--

-- Used to hook the tooltip to avoid the full tooltip function
function KindredArtifactTracking:TooltipHook()
    local name, unit = GameTooltip:GetUnit();
    local guid = false;

    if unit then
        guid = UnitGUID(unit);
    elseif name then
        guid = self:NameToGUID(name);
    end
    if self:IsGUID(guid, 'Player') and self.db.profile.showOnTooltip then
        self:ShowTooltip(guid, unit);
    end
end

function KindredArtifactTracking:NameToGUID(name, realm)
    if name then
        return UnitGUID(name)
    end
    return false;
end

function KindredArtifactTracking:IsGUID(guid, type)
    if not guid then return false end
    if not type then type = 'player' end

    local gType, gGuid = strsplit('-', guid, 2);

    if strlower(type) == strlower(gType) then
        return true
    else
        return false
    end
end

function KindredArtifactTracking:ShowTooltip(guid, unit)
    if InCombatLockdown() then return end

    if not guid then
        guid = UnitGUID("mouseover")
        if self:IsGUID(guid, "player") then
            unit = UnitId("mouseover")
        end
    end

    local weapons, knowledge = self:GetArtifactWeapons(guid, unit)

    -- Artifact Weapons
    local noArtifactInfo = true
    if weapons then
        for k,v in pairs(weapons) do
            -- Build the tooltip text
            noArtifactInfo = false
            local traitsHex, r, g, b = KL.getColorFromScale(v.numRanksPurchased, 34)
            local goldTraits = self:GetGoldTraitCount(v.traits)
            local textLeft = '|cFF216bff' .. v.name .. ':|r '
            local textRight = '|cFF' .. traitsHex .. ' ' .. v.numRanksPurchased .. '|r |cFFFFD700(' .. goldTraits .. ')|r |cFFFFFFFF' .. L.core.traitsUnlocked .. '|r'
            self:AddTooltipText(textLeft, textRight);
        end
    else
        noArtifactInfo = true
    end

    if noArtifactInfo then
        local textLeft = '|cFF216bff' .. L.core.artifactInformationUnavailable .. '|r '
        self:AddTooltipText(textLeft, '')
        if self.MyUnitGUID == guid then
            self:UpdateArtifacts(true)
        else
            self:SendArtifactRequestToTarget(unit)
        end
    end
    -- Artifact Knowledge
    if knowledge and knowledge['knowledgeLevel'] and knowledge['knowledgeMultiplier'] then
        local textLeft = '|cFF216bff' .. L.core.akLabel .. ':|r '
        local textRight = knowledge['knowledgeLevel'] .. ' (' .. (knowledge['knowledgeMultiplier'] - 1) * 100 .. '%)'
        self:AddTooltipText(textLeft, textRight)
    end
    return true;
end

-- Return the hex, r, g, b of a score
function KindredArtifactTracking:ColorArtifactTraits(traits)
    return KL.getColorFromScale(traits, 34)
end

-- Add lines to the tooltip, testLeft must be the same
function KindredArtifactTracking:AddTooltipText(textLeft, textRight)

    -- Loop tooltip text to check if its alredy there
    local ttLines = GameTooltip:NumLines();
    local ttUpdated = false;

    for i = 1,ttLines do

        -- If the static text matches
        if _G["GameTooltipTextLeft"..i]:GetText() == textLeft then

            -- Update the text
            _G["GameTooltipTextLeft"..i]:SetText(textLeft);
            _G["GameTooltipTextRight"..i]:SetText(textRight);
            GameTooltip:Show();

            -- Remember that we have updated the tool tip so we won't again
            ttUpdated = true;
            break;
        end
    end

    -- Tooltip is new
    if not ttUpdated then
        GameTooltip:AddDoubleLine(textLeft, textRight);
        GameTooltip:Show();
    end
end

--
-- End Credited Code
--

function KindredArtifactTracking:CreateCachedArtifactsPanel()
        cacheFrame:ClearAllPoints()
    cacheFrame:SetPoint("CENTER", UIParent)
    cacheFrame:SetHeight(480)
    cacheFrame:SetWidth(640)

    cacheFrame:SetBackdrop({
        bgFile="Interface/DialogFrame/UI-DialogBox-Background",
        edgeFile="Interface/DialogFrame/UI-DialogBox-Border",
        tile=1, tileSize=32, edgeSize=32,
        insets={left=11, right=12, top=12, bottom=11}
    })

    cacheFrame.Title = cacheFrame:CreateFontString(nil, "OVERLAY", "GameFontNormal")
    cacheFrame.Title:SetPoint("TOP", cacheFrame, "TOP", 6, -15)
    cacheFrame.Title:SetTextColor(0.0, 1.0, 1.0, 1.0)
    cacheFrame.Title:SetText("Cached Artifact Information")

    cacheFrame.EditBox = CreateFrame("EditBox", nil, cacheFrame)
    cacheFrame.EditBox:SetWidth(600)
    cacheFrame.EditBox:SetHeight(380)
    cacheFrame.EditBox:SetPoint("CENTER", cacheFrame, "CENTER", 30, 0)
    cacheFrame.EditBox:SetFontObject("GameFontNormal")
    cacheFrame.EditBox:SetMultiLine(true)
    cacheFrame.EditBox:SetAutoFocus(false)
    cacheFrame.EditBox:SetScript("OnEscapePressed", function() cacheFrame.EditBox:ClearFocus() end)

    cacheFrame.EditBox.Scroll = CreateFrame('ScrollFrame', nil, cacheFrame, 'UIPanelScrollFrameTemplate')
    cacheFrame.EditBox.Scroll:SetPoint('TOPLEFT', cacheFrame, 'TOPLEFT', 30, -35)
    cacheFrame.EditBox.Scroll:SetPoint('BOTTOMRIGHT', cacheFrame, 'BOTTOMRIGHT', -35, 30)
    cacheFrame.EditBox.Scroll:SetScrollChild(cacheFrame.EditBox)

    cacheFrame.Button = CreateFrame("Button", nil, cacheFrame, "OptionsButtonTemplate")
    cacheFrame.Button:SetWidth(80)
    cacheFrame.Button:SetHeight(24)
    cacheFrame.Button:SetPoint("CENTER", cacheFrame, "BOTTOM", 0, 25)
    cacheFrame.Button:SetScript("OnClick",function() cacheFrame:Hide() end)
    cacheFrame.Button:SetText("Close")
end

function KindredArtifactTracking:ShowCachedArtifacts()
    cacheFrame.EditBox:SetText(KL.PrintR(self.db.global.artifactCache, true))
    cacheFrame:Show()
end

function KindredArtifactTracking:CreateDebugPanel()
    debugFrame:ClearAllPoints()
    debugFrame:SetPoint("CENTER", UIParent)
    debugFrame:SetHeight(480)
    debugFrame:SetWidth(640)

    debugFrame:SetBackdrop({
        bgFile="Interface/DialogFrame/UI-DialogBox-Background",
        edgeFile="Interface/DialogFrame/UI-DialogBox-Border",
        tile=1, tileSize=32, edgeSize=32,
        insets={left=11, right=12, top=12, bottom=11}
    })

    debugFrame.Title = debugFrame:CreateFontString(nil, "OVERLAY", "GameFontNormal")
    debugFrame.Title:SetPoint("TOP", debugFrame, "TOP", 6, -15)
    debugFrame.Title:SetTextColor(0.0, 1.0, 1.0, 1.0)
    debugFrame.Title:SetText("Logged Debug Information")

    debugFrame.EventPanel = CreateFrame("SimpleHTML", nil, debugFrame)
    debugFrame.EventPanel:SetWidth(600)
    debugFrame.EventPanel:SetHeight(380)
    debugFrame.EventPanel:SetPoint("CENTER", debugFrame, "CENTER", 0, 0)
    debugFrame.EventPanel:SetFontObject("GameFontNormal")
    debugFrame.EventPanel:SetHyperlinksEnabled(true)
    debugFrame.EventPanel:SetScript("OnHyperlinkClick",
        function(slf, linkData, link, button)
            local index = gsub(linkData, "kUtilsDebug:", "")
            self:ShowMessageDetailFrame(index)
        end)

    --debugFrame.EventPanel:SetBackdrop({
    --    bgFile="Interface/DialogFrame/UI-DialogBox-Background",
    --    --edgeFile="Interface/DialogFrame/UI-DialogBox-Border",
    --    tile=1, tileSize=32, edgeSize=32,
    --    insets={left=11, right=12, top=12, bottom=11}
    --})

    debugFrame.EventPanel.Scroll = CreateFrame('ScrollFrame', nil, debugFrame, 'UIPanelScrollFrameTemplate')
    debugFrame.EventPanel.Scroll:SetPoint('TOPLEFT', debugFrame, 'TOPLEFT', 30, -35)
    debugFrame.EventPanel.Scroll:SetPoint('BOTTOMRIGHT', debugFrame, 'BOTTOMRIGHT', -35, 30)
    debugFrame.EventPanel.Scroll:SetScrollChild(debugFrame.EventPanel)

    debugFrame.CloseButton = CreateFrame("Button", nil, debugFrame, "OptionsButtonTemplate")
    debugFrame.CloseButton:SetWidth(80)
    debugFrame.CloseButton:SetHeight(24)
    debugFrame.CloseButton:SetPoint("CENTER", debugFrame, "BOTTOM", -85, 25)
    debugFrame.CloseButton:SetScript("OnClick",function() debugFrame:Hide() end)
    debugFrame.CloseButton:SetText("Close")

    debugFrame.RefreshButton = CreateFrame("Button", nil, debugFrame, "OptionsButtonTemplate")
    debugFrame.RefreshButton:SetWidth(80)
    debugFrame.RefreshButton:SetHeight(24)
    debugFrame.RefreshButton:SetPoint("CENTER", debugFrame, "BOTTOM", 0, 25)
    debugFrame.RefreshButton:SetScript("OnClick",function() debugFrame:Hide() self:ShowDebugPanel() end)
    debugFrame.RefreshButton:SetText("Refresh")

    debugFrame.ClearButton = CreateFrame("Button", nil, debugFrame, "OptionsButtonTemplate")
    debugFrame.ClearButton:SetWidth(80)
    debugFrame.ClearButton:SetHeight(24)
    debugFrame.ClearButton:SetPoint("CENTER", debugFrame, "BOTTOM", 85, 25)
    debugFrame.ClearButton:SetScript("OnClick",function() debugFrame:Hide(); self:ClearDebug(); self:ShowDebugPanel() end)
    debugFrame.ClearButton:SetText("Clear")
end

function KindredArtifactTracking:ShowDebugPanel()
    debugFrame.EventPanel:SetText(self:GetEventHtml())
    debugFrame:Show()
end

function KindredArtifactTracking:ShowMessageDetailFrame(index)
    debugFrame:Hide()
    local data = receivedMessages[tonumber(index)]
    messageDetailFrame.Sender:SetText("|cFFFFD700From:|r " .. data.from)
    messageDetailFrame.Prefix:SetText("|cFFFFD700Message Prefix:|r " .. data.pfx)
    messageDetailFrame.Distribution:SetText("|cFFFFD700Message Distribution Method:|r " .. data.pfx)
    messageDetailFrame.Received:SetText("|cFFFFD700Logged At:|r " .. date("%X %x", data.rcvd))
    messageDetailFrame.EditBox:SetText(data.msg)
    messageDetailFrame:Show()
end

function KindredArtifactTracking:CreateMessageDetailPane()
    messageDetailFrame:ClearAllPoints()
    messageDetailFrame:SetPoint("CENTER", UIParent)
    messageDetailFrame:SetHeight(480)
    messageDetailFrame:SetWidth(640)

    messageDetailFrame:SetBackdrop({
        bgFile="Interface/DialogFrame/UI-DialogBox-Background",
        edgeFile="Interface/DialogFrame/UI-DialogBox-Border",
        tile=1, tileSize=32, edgeSize=32,
        insets={left=11, right=12, top=12, bottom=11}
    })

    messageDetailFrame.Title = messageDetailFrame:CreateFontString(nil, "OVERLAY", "GameFontNormal")
    messageDetailFrame.Title:SetPoint("TOP", messageDetailFrame, "TOP", 6, -15)
    messageDetailFrame.Title:SetTextColor(0.0, 1.0, 1.0, 1.0)
    messageDetailFrame.Title:SetText("Message Details")

    messageDetailFrame.Sender = messageDetailFrame:CreateFontString(nil, "OVERLAY", "GameFontNormal")
    messageDetailFrame.Sender:SetTextColor(1.0, 1.0, 1.0, 1.0)
    messageDetailFrame.Sender:SetPoint("TOPLEFT", messageDetailFrame, "TOPLEFT", 12, -10)

    messageDetailFrame.Prefix = messageDetailFrame:CreateFontString(nil, "OVERLAY", "GameFontNormal")
    messageDetailFrame.Prefix:SetTextColor(1.0, 1.0, 1.0, 1.0)
    messageDetailFrame.Prefix:SetPoint("TOPLEFT", messageDetailFrame, "TOPLEFT", 12, -20)

    messageDetailFrame.Received = messageDetailFrame:CreateFontString(nil, "OVERLAY", "GameFontNormal")
    messageDetailFrame.Received:SetTextColor(1.0, 1.0, 1.0, 1.0)
    messageDetailFrame.Received:SetPoint("TOPLEFT", messageDetailFrame, "TOPLEFT", 12, -30)

    messageDetailFrame.Distribution = messageDetailFrame:CreateFontString(nil, "OVERLAY", "GameFontNormal")
    messageDetailFrame.Distribution:SetTextColor(1.0, 1.0, 1.0, 1.0)
    messageDetailFrame.Distribution:SetPoint("TOPLEFT", messageDetailFrame, "TOPLEFT", 12, -40)

    messageDetailFrame.Message = messageDetailFrame:CreateFontString(nil, "OVERLAY", "GameFontNormal")
    messageDetailFrame.Message:SetTextColor(1.0, 1.0, 1.0, 1.0)
    messageDetailFrame.Message:SetPoint("TOPLEFT", messageDetailFrame, "TOPLEFT", 12, -50)
    messageDetailFrame.Message:SetText("|cFFFFD700Message:|r ")

    messageDetailFrame.EditBox = CreateFrame("EditBox", nil, messageDetailFrame)
    messageDetailFrame.EditBox:SetWidth(600)
    messageDetailFrame.EditBox:SetHeight(380)
    messageDetailFrame.EditBox:SetPoint("TOPLEFT", messageDetailFrame, "TOPLEFT", 12, -60)
    messageDetailFrame.EditBox:SetFontObject("GameFontNormal")
    messageDetailFrame.EditBox:SetTextColor(1.0, 1.0, 1.0, 1.0)
    messageDetailFrame.EditBox:SetMultiLine(true)
    messageDetailFrame.EditBox:SetAutoFocus(false)
    messageDetailFrame.EditBox:SetScript("OnEscapePressed", function() messageDetailFrame.EditBox:ClearFocus() end)

    messageDetailFrame.EditBox.Scroll = CreateFrame('ScrollFrame', nil, messageDetailFrame, 'UIPanelScrollFrameTemplate')
    messageDetailFrame.EditBox.Scroll:SetPoint('TOPLEFT', messageDetailFrame, 'TOPLEFT', 12, -60)
    messageDetailFrame.EditBox.Scroll:SetPoint('BOTTOMRIGHT', messageDetailFrame, 'BOTTOMRIGHT', -35, 30)
    messageDetailFrame.EditBox.Scroll:SetScrollChild(messageDetailFrame.EditBox)

    messageDetailFrame.Button = CreateFrame("Button", nil, messageDetailFrame, "OptionsButtonTemplate")
    messageDetailFrame.Button:SetWidth(80)
    messageDetailFrame.Button:SetHeight(24)
    messageDetailFrame.Button:SetPoint("CENTER", messageDetailFrame, "BOTTOM", 0, 25)
    messageDetailFrame.Button:SetScript("OnClick",function() messageDetailFrame:Hide(); debugFrame:Show() end)
    messageDetailFrame.Button:SetText("Back")
end

function KindredArtifactTracking:GetEventHtml()
    local htmlText = "<html><body>"
    for k,v in pairs(receivedMessages) do
        htmlText = htmlText .. "<p>|cFFFFD700Sender:|r |cFFFFFFFF" .. v.from ..
                               " |r|cFFFFD700Message Type:|r |cFFFFFFFF" .. v.pfx ..
                               " |r|cFFFFD700Logged At|r: |cFFFFFFFF" .. date("%X %x", v.rcvd) ..
                               " |r|cFFFFD700Distribution:|r |cFFFFFFFF" .. v.dist ..
                               " |r|cFFFFD700Details:|r |HkUtilsDebug:" .. k .. "|h|cFF216bff[Click]|r|h</p>"
    end
    return htmlText .. "</body></html>"
end

function KindredArtifactTracking:SerializeArtifacts()
    if self.MyUnitGUID == nil then
        self.MyUnitGUID = self:NameToGUID(UnitName("player"))
    end
    if self.MyUnitGUID == nil then return end
    local ret = {}
    ret['GUID'] = self.MyUnitGUID
    ret['artifacts'] = self.db.global.artifactCache[self.MyUnitGUID]
    return self:Serialize(ret)
end

function KindredArtifactTracking:SendArtifactRequestToTarget(unit)
    if requestTargets == nil then requestTargets = {} end
    if requestTargets[UnitName(unit)] == nil then
        requestTargets[UnitName(unit)] = true
        local messageMeta = {
            prefix = KUTIL_prefix .. KUTIL_prefixRequestArtifacts,
            message = '',
            channel = "WHISPER",
            target = UnitName(unit),
            priority = "NORMAL"
        }
        messageQueue[len(messageQueue) + 1] = messageMeta
    end
end

function KindredArtifactTracking:SendArtifactsToTarget(target)
    if sendTargets == nil then sendTargets = {} end
    if sendTargets[target] == nil then
        sendTargets[target] = true
        local messageMeta = {
            prefix = KUTIL_prefix .. KUTIL_prefixArtifacts,
            message = self:SerializeArtifacts(),
            channel = "WHISPER",
            target = target,
            priority = "NORMAL"
        }
        messageQueue[len(messageQueue) + 1] = messageMeta
    end
end

function KindredArtifactTracking:SendMessage()
    if len(messageQueue) > 0 then
        local message = table.remove(messageQueue, 1)
        if message.target and playersWithAddon[message.target] == true then
            if message.prefix == KUTIL_prefix .. KUTIL_prefixArtifacts then
                if sendTargets ~= nil then
                    sendTargets[message.target] = nil
                end
            elseif message.prefix == KUTIL_prefix .. KUTIL_prefixRequestArtifacts then
                if requestTargets ~= nil then
                    requestTargets[message.target] = nil
                end
            else
                if self.db.profile.enableDebug then
                    debugInfo = {
                        from = 'Self - Error Sending Message',
                        pfx = "N/A",
                        dist = "N/A",
                        msg = "Bad prefix " .. message.prefix .. " in message, not sending.",
                        rcvd = time()
                    }
                    receivedMessages[len(receivedMessages) + 1] = debugInfo
                end
            end
            if self.db.profile.enableDebug then
                local dst = message.channel
                if message.target then
                    dst = dst .. ' - ' .. message.target
                end
                local debugInfo = {
                    from = 'Self - Sending Message',
                    pfx = message.prefix,
                    dist = dst,
                    msg = message.message,
                    rcvd = time()
                }
                receivedMessages[len(receivedMessages) + 1] = debugInfo
            end
            self:SendCommMessage(message.prefix, message.message, message.channel, message.target, message.priority)
        elseif message.prefix == KUTIL_prefix then
            if time() - lastBroadcastHaveAddon[message.channel] > 5 then
                if self.db.profile.enableDebug then
                    local dst = message.channel
                    if message.target then
                        dst = dst .. ' - ' .. message.target
                    end
                    local debugInfo = {
                        from = 'Self - Sending "I have KindredUtil" message',
                        pfx = message.prefix,
                        dist = dst,
                        msg = message.message,
                        rcvd = time()
                    }
                    receivedMessages[len(receivedMessages) + 1] = debugInfo
                end
                self:SendCommMessage(message.prefix, message.message, message.channel, message.target, message.priority)
                lastBroadcastHaveAddon[message.channel] = time()
            end
        end
    end
end

function KindredArtifactTracking:BroadcastHaveAddon(guild)
    local messageMeta = {
        prefix = KUTIL_prefix,
        message = 'I Have KindredArtifactTracking',
        channel = "GUILD",
        target = nil,
        priority = "NORMAL"
    }
    if guild ~= nil then
        messageQueue[len(messageQueue) + 1] = messageMeta
    else
        messageQueue[len(messageQueue) + 1] = messageMeta
        messageMeta.channel = "RAID"
        messageQueue[len(messageQueue) + 1] = messageMeta
    end
end

function KindredArtifactTracking:OnCommReceived(prefix, message, distribution, sender)
    if self.db.profile.enableDebug then
        local debugInfo = {
            from = sender,
            pfx = prefix,
            dist = distribution,
            msg = KL.PrintR(message, true),
            rcvd = time()
        }
        receivedMessages[len(receivedMessages) + 1] = debugInfo
    end
    if sender ~= UnitName("player") then
        if prefix == KUTIL_prefix .. KUTIL_prefixArtifacts then
            local success, payload = self:Deserialize(message)
            if success and payload.GUID then
                local thisPersonsArtifacts = {}
                if self.db.global.artifactCache[payload.GUID] then
                    thisPersonsArtifacts = self.db.global.artifactCache[payload.GUID]
                end
                if payload.artifacts then
                    for k, v in pairs(payload.artifacts) do
                        thisPersonsArtifacts[k] = v
                    end
                    thisPersonsArtifacts.lastUpdated = time()
                    self.db.global.artifactCache[payload.GUID] = thisPersonsArtifacts
                else
                    if self.db.profile.enableDebug then
                        local debugInfo = {
                            from = 'Self - Recv Error',
                            pfx = "N/A",
                            dist = "N/A",
                            msg = "Bad payload received. No Artifact Info in message",
                            rcvd = time()
                        }
                        receivedMessages[len(receivedMessages) + 1] = debugInfo
                    end
                end
            else
                if self.db.profile.enableDebug then
                    local debugInfo = {
                        from = 'Self - Recv Error',
                        pfx = "N/A",
                        dist = "N/A",
                        msg = "Bad payload received. No Character GUID in message",
                        rcvd = time()
                    }
                    receivedMessages[len(receivedMessages) + 1] = debugInfo
                end
            end
        elseif prefix == KUTIL_prefix then
            playersWithAddon[sender] = true
            if self.db.profile.enableDebug then
                local debugInfo = {
                    from = 'Self - Add player with addon',
                    pfx = "N/A",
                    dist = "N/A",
                    msg = "Player " .. sender .. " has KindredArtifactTracking installed",
                    rcvd = time()
                }
                receivedMessages[len(receivedMessages) + 1] = debugInfo
            end
        elseif prefix == KUTIL_prefix .. KUTIL_prefixRequestArtifacts then
            self:SendArtifactsToTarget(sender)
        end
    end
end

function KindredArtifactTracking:RegisterPaperdoll()
    table.insert(PAPERDOLL_STATCATEGORIES[1].stats, 1, {
          stat = 'KUTIL_ak',
    });
        table.insert(PAPERDOLL_STATCATEGORIES[1].stats, 1, {
          stat = 'KUTIL_atu',
    });
    PAPERDOLL_STATINFO['KUTIL_ak'] = { updateFunc = function(...) KindredArtifactTracking:UpdatePaperDollFrameAk(...); end };
    PAPERDOLL_STATINFO['KUTIL_atu'] = { updateFunc = function(...) KindredArtifactTracking:UpdatePaperDollFrameAtu(...); end };
end

function KindredArtifactTracking:UnregisterPaperdoll()
    table.foreach(PAPERDOLL_STATCATEGORIES[1].stats, function(k, v)
        if v.stat == 'KUTIL_ak' then
            table.remove(PAPERDOLL_STATCATEGORIES[1].stats, k)
        elseif v.stat == 'KUTIL_atu' then
            table.remove(PAPERDOLL_STATCATEGORIES[1].stats, k)
        end
    end)
    PAPERDOLL_STATINFO['KUTIL_ak'] = { updateFunc = function(...) return false; end };
    PAPERDOLL_STATINFO['KUTIL_atu'] = { updateFunc = function(...) return false; end };
end

function KindredArtifactTracking:UpdatePaperDollFrameAtu(statFrame, unit)
    local artifactName, artifactTraits, knowledge, goldTraits = self:GetCurrentArtifactInfo()
    if artifactName and artifactTraits then
        local traitsHex = self:ColorArtifactTraits(artifactTraits)
        local formated = '|cFF' .. traitsHex .. artifactTraits .. '|r'
        local goldFormated =  '|cFFFFD700(' .. goldTraits .. ')|r'
        PaperDollFrame_SetLabelAndText(statFrame, L.core.atuLabel, formated .. ' ' .. goldFormated, false);
        statFrame.tooltip = HIGHLIGHT_FONT_COLOR_CODE..L.core.name..' - '..L.core.atuLabel..FONT_COLOR_CODE_CLOSE;
        statFrame.tooltip2 = formated .. '|cFFFFFFFF' .. L.core.atuLabel .. '|r\n|cFFFFD700' .. goldTraits .. '|r |cFFFFFFFF' .. L.core.gatuLabel .. '|r\n\n' .. L.core.atuDesc;
    else
        local formated = '|cFFFFFFFF' .. L.core.loading .. '|r'
        PaperDollFrame_SetLabelAndText(statFrame, L.core.atuLabel, formated, false);
        statFrame.tooltip = HIGHLIGHT_FONT_COLOR_CODE..L.core.name..' - '..L.core.atuLabel..FONT_COLOR_CODE_CLOSE;
        statFrame.tooltip2 = L.core.atuDesc;
        self:UpdateArtifacts(true)
    end
    statFrame:Show();
end

function KindredArtifactTracking:UpdatePaperDollFrameAk(statFrame, unit)
    local artifactName, artifactTraits = self:GetCurrentArtifactInfo()
    local ak, akm = LA.GetArtifactKnowledge()
    if ak == -1 then ak = 'Unknown' end
    if akm == 0 then akm = 'Unknown Multiplier' else akm = (akm - 1) * 100  .. '%' end

    PaperDollFrame_SetLabelAndText(statFrame, L.core.akLabel, ak, false);
    statFrame.tooltip = HIGHLIGHT_FONT_COLOR_CODE..L.core.name..' - '..L.core.akLabel..FONT_COLOR_CODE_CLOSE;
    statFrame.tooltip2 = akm .. '\n\n' .. L.core.akDesc;
    statFrame:Show();
end

function KindredArtifactTracking:OnInitialize()
    -- Code that runs when the addon is first loaded
    _G.print("KindredArtifactTracking Loaded.")
    GameTooltip:HookScript("OnTooltipSetUnit", function(...) KindredArtifactTracking:TooltipHook(...); end);
    self.db = LibStub("AceDB-3.0"):New("KindredDB")
    KATRACK_Options.args.profile = LibStub("AceDBOptions-3.0"):GetOptionsTable(self.db)
    LibStub("AceConfig-3.0"):RegisterOptionsTable("KindredArtifactTracking", KATRACK_Options, {"KindredArtifactTracking", "kUtils"})
    self.optionsFrame = LibStub("AceConfigDialog-3.0"):AddToBlizOptions("KindredArtifactTracking")
    self:RegisterComm(KUTIL_prefix)
    self:RegisterComm(KUTIL_prefix .. KUTIL_prefixArtifacts)
    self:RegisterComm(KUTIL_prefix .. KUTIL_prefixRequestArtifacts)
    -- Add to Paperdoll
    if self.db.profile.showOnPaperDoll then
        self:RegisterPaperdoll();
    end
    self:CreateCachedArtifactsPanel()
    self:CreateDebugPanel()
    self:CreateMessageDetailPane()
end

function KindredArtifactTracking:OnEnable()
    -- Code that runs when the addon is enabled
    if self.db.global.artifactCache == nil then
        self.db.global.artifactCache = {}
    end
    if self.MyUnitGUID == nil then
        self.MyUnitGUID = self:NameToGUID(UnitName("player"))
    end

    -- Register Party Changes
    self:RegisterEvent("GROUP_ROSTER_UPDATE", function() self:BroadcastHaveAddon() end)

    -- Register Chat Commands
    self:RegisterChatCommand("kindredClearCache", "ClearCache")
    self:RegisterChatCommand("kindredForceUpdate", "UpdateArtifacts")
    self:RegisterChatCommand("kindredShowCache", "ShowCachedArtifacts")
    self:RegisterChatCommand("kindredDebug", "ShowDebugPanel")

    -- Update artifacts and then send messages when certain events happen
    LA.RegisterCallback(self, "ARTIFACT_ADDED", "UpdateArtifacts")
    LA.RegisterCallback(self, "ARTIFACT_ACTIVE_CHANGED", "UpdateArtifacts")
    LA.RegisterCallback(self, "ARTIFACT_EQUIPPED_CHANGED", "UpdateArtifacts")
    LA.RegisterCallback(self, "ARTIFACT_KNOWLEDGE_CHANGED", "UpdateArtifacts")
    LA.RegisterCallback(self, "ARTIFACT_POWER_CHANGED", "UpdateArtifacts")
    LA.RegisterCallback(self, "ARTIFACT_RELIC_CHANGED", "UpdateArtifacts")
    LA.RegisterCallback(self, "ARTIFACT_TRAITS_CHANGED", "UpdateArtifacts")

    -- Set sensable default settings
    if self.db.profile.addonMessageDelay == nil then
        self.db.profile.addonMessageDelay = 1
    end
    if self.db.profile.showOnTooltip == nil then
        self.db.profile.showOnTooltip = 1
    end
    if self.db.profile.showOnPaperDoll == nil then
        self.db.profile.showOnPaperDoll = 1
    end
    if self.db.profile.enableDebug == nil then
        self.db.profile.enableDebug = false
    end

    -- Register the timer for sending messages
    self:ScheduleRepeatingTimer("SendMessage", tonumber(self.db.profile.addonMessageDelay))

    self:UpdateArtifacts()

    self:BroadcastHaveAddon("guild")

    --messageQueue = {}
    --sendTargets = {}
    --requestTargets = {}
    --playersWithAddon = {}
end

function KindredArtifactTracking:OnDisable()
    -- Code that runs when the addon is disabled
    -- Unregister Chat Commands
    self:UnregisterChatCommand("kindredClearCache")
    self:UnregisterChatCommand("kindredForceUpdate")
    self:UnregisterChatCommand("kindredShowCache")
    self:UnregisterChatCommand("kindredDebug")

    -- Unregister Callbacks
    LA.UnregisterCallback(self, "ARTIFACT_ADDED")
    LA.UnregisterCallback(self, "ARTIFACT_ACTIVE_CHANGED")
    LA.UnregisterCallback(self, "ARTIFACT_EQUIPPED_CHANGED")
    LA.UnregisterCallback(self, "ARTIFACT_KNOWLEDGE_CHANGED")
    LA.UnregisterCallback(self, "ARTIFACT_POWER_CHANGED")
    LA.UnregisterCallback(self, "ARTIFACT_RELIC_CHANGED")
    LA.UnregisterCallback(self, "ARTIFACT_TRAITS_CHANGED")

    -- Unregister Paperdoll Handler
    self:UnregisterPaperdoll()

    -- Unregister all timers
    self:CancelAllTimers()

    messageQueue = nil
    sendTargets = {}
    requestTargets = {}
    playersWithAddon = {}
end
