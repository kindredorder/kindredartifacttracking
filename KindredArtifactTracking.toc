## Interface: 72000
## Title: KindredArtifactTracking
## Notes: A collection of utilities developed for the Kindred Guild on Malygos(US)
## Author: Timetick <Kindred> US/Malygos
## Version: @project-version@
## SavedVariables: KindredDB
## Dependencies: KindredLibs, LibArtifactData-1.0

embeds.xml
KindredArtifactTracking.xml

