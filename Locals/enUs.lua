-- Default localization - enUS
local L = LibStub("AceLocale-3.0"):NewLocale("KindredArtifactTracking", "enUS", true);

L.core = {
	name = "Kindred Artifact Tracking",
	tooltipTraitsUnlocked = "",
	artifactInformationUnavailable = "No Artifact Information Available",
	traitsUnlocked = "traits unlocked",
	atuLabel = "Artifact Traits Unlocked",
	atuDesc = "This is the number of artifact traits unlocked on your current weapon.",
	gatuLabel = "Gold Artifact Traits Unlocked",
	akLabel = "Artifact Knowledge",
	akDesc = "This is your current artifact Knowledge",
	loading = 'Loading...'
}
