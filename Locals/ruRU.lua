-- localization - ruRU
local L = LibStub("AceLocale-3.0"):NewLocale("KindredArtifactTracking", "ruRU");

if not L then return end

L.core = {
	name = "Kindred Artifact Tracking",
	tooltipTraitsUnlocked = "",
	artifactInformationUnavailable = "Нет информации об Артефакте.",
	traitsUnlocked = "", -- not necessary. Everything is clear without the text.
	atuLabel = "Разблок. трейтов",
	atuDesc = "Число разблокированных трейтов Артефакта.",
	gatuLabel = "Основные трейты Артефакта.",
	akLabel = "Знание Артефакта",
	akDesc = "Текущее Знание Артефакта.",
	loading = 'Загрузка...'
}
